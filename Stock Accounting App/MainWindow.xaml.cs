﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Stock_Accounting_App
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ScrollViewer scrollView;

        public ListView listView;

        private GridView gridView;

        private Dictionary<string , GridViewColumn> columns;

        public Button addButton;

        /// <summary>
        /// Main window constructor 
        /// Building user interface
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            scrollView = new ScrollViewer() { Height = this.Height - 110, Margin = new Thickness(0, 20, 0, 0), VerticalAlignment = VerticalAlignment.Top };

            listView = new ListView() { ContextMenu = new ItemContextMenu() };        

            gridView = new GridView();

            columns = new Dictionary<string, GridViewColumn>();

            columns["Product"] = new GridViewColumn() { Header = "Product", DisplayMemberBinding = new Binding("Product"), Width = this.Width / 6 };
            columns["Price"] = new GridViewColumn() { Header = "Price", DisplayMemberBinding = new Binding("Price"), Width = this.Width / 6 };
            columns["Weight"] = new GridViewColumn() { Header = "Weight", DisplayMemberBinding = new Binding("Weight"), Width = this.Width / 6 };
            columns["Arrival"] = new GridViewColumn() { Header = "Arrival", DisplayMemberBinding = new Binding("Arrival"), Width = this.Width / 6 };
            columns["Exportation"] = new GridViewColumn() { Header = "Exportation", DisplayMemberBinding = new Binding("Exportation"), Width = this.Width / 6 };
            columns["Provider"] = new GridViewColumn() { Header = "Provider", DisplayMemberBinding = new Binding("Provider"), Width = this.Width / 6 };

            //Adding headers to table
            columns.ToList().ForEach((column) => gridView.Columns.Add(column.Value));

            listView.View = gridView;

            //Output items
            foreach (var row in File.ReadAllLines("Table.txt"))
            {
                var parameters = row.Split(new string[] { "\t" }, StringSplitOptions.None);
               
                listView.Items.Add(new Item() { Product = parameters[0], Price = parameters[1], Weight = parameters[2], Arrival = parameters[3], Exportation = parameters[4], Provider = parameters[5]  });
               
            }

            addButton = new Button() { Content = new TextBlock() { Text = "Add new item" }, Height = 32, Margin = new Thickness(25, 0, 25, 10), Width = 700, VerticalAlignment = VerticalAlignment.Bottom };        

            //Handling add button click
            addButton.Click += (object sender, RoutedEventArgs e) => { Grid.Children.Remove(addButton); Grid.Children.Add(new InputField()); };

            //Handling menu
            sortByPrice.Click += (object sender, RoutedEventArgs e) => sort(SortType.byPrice);
            sortByWeight.Click += (object sender, RoutedEventArgs e) => sort(SortType.byWeight);
            sortByArrival.Click += (object sender, RoutedEventArgs e) => sort(SortType.byArrival);
            sortByExport.Click += (object sender, RoutedEventArgs e) => sort(SortType.byExport);

            //Adding all in main grid
            scrollView.Content = listView;

            Grid.Children.Add(addButton);

            Grid.Children.Add(scrollView);

            //Saving ref of ui elements in global
            Global.mainGrid = this.Grid;
            Global.listView = this.listView;
            Global.addButton = this.addButton;
            Global.mainWindow = this;
        }

        private void sort(SortType type)
        {
            List<Item> list = new List<Item>();

            foreach (var item in Global.listView.Items)
            {
                var row = item as Item;
                list.Add(row);
            }

            listView.Items.Clear();

            switch (type)
            {
                case SortType.byPrice:
                    list.Sort((x, y) => int.Parse(x.Price).CompareTo(int.Parse(y.Price)));
                    break;
                case SortType.byWeight:
                    list.Sort((x, y) => int.Parse(x.Weight).CompareTo(int.Parse(y.Weight)));
                    break;
                case SortType.byArrival:
                    list.Sort((x, y) => Convert.ToDateTime(x.Arrival).CompareTo(Convert.ToDateTime(y.Arrival)));
                    break;
                case SortType.byExport:
                    list.Sort((x, y) => Convert.ToDateTime(x.Exportation).CompareTo(Convert.ToDateTime(y.Exportation)));
                    break;
            }

            list.Reverse();

            foreach (var item in list)
                listView.Items.Add(item);           
        } 
        
        private void clear()
        {
            listView.Items.Clear();

            StreamWriter fileWriter = new StreamWriter("Table.txt", false);

            foreach (var item in Global.listView.Items)
            {
                var row = item as Item;
                fileWriter.WriteLine(row.Product + "\t" + row.Price + "\t" + row.Weight + "\t" + row.Arrival + "\t" + row.Exportation + "\t" + row.Provider);
            }

            fileWriter.Close();
        }
    }

    public enum SortType
    {
        byPrice,
        byWeight,
        byArrival,
        byExport
    }
}
