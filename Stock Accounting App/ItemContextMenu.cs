﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Stock_Accounting_App
{
    /// <summary>
    /// Context menu for deleting item from table
    /// </summary>
    class ItemContextMenu : ContextMenu
    {
        public MenuItem deleteItem;

        public MenuItem editItem;

        public ItemContextMenu()
        {
            deleteItem = new MenuItem() { Header = "Delete item" };

            editItem = new MenuItem() { Header = "Edit item" };

            deleteItem.Click += (object sender, RoutedEventArgs e) => { Global.listView.Items.Remove(Global.listView.SelectedItem); deleteData();};

            editItem.Click += (object sender, RoutedEventArgs e) => 
            {
                
                Global.mainGrid.Children.Remove(Global.mainWindow.addButton);
                Global.mainGrid.Children.Add(new EditInputField((Item)Global.listView.SelectedItem));
                Global.listView.Items.Remove(Global.listView.SelectedItem);
                deleteData();

            };

            this.Items.Add(deleteItem);

            this.Items.Add(editItem);
        }

        /// <summary>
        /// Deleting item from file
        /// </summary>
        public void deleteData()
        {      
            StreamWriter fileWriter = new StreamWriter("Table.txt", false);

            foreach(var item in Global.listView.Items)
            {
                var row = item as Item;
                fileWriter.WriteLine(row.Product + "\t" + row.Price + "\t" + row.Weight + "\t" + row.Arrival + "\t" + row.Exportation + "\t" + row.Provider);
            }
                
            fileWriter.Close();
        }
    }
}
