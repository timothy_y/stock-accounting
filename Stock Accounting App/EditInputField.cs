﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Stock_Accounting_App
{
    class EditInputField : InputField
    {
        public EditInputField(Item item)
        {
            productBox.Text = item.Product;
            priceBox.Text = item.Price;
            weightBox.Text = item.Weight;
            arrivalBox.Text = item.Arrival;
            exportationBox.Text = item.Exportation;
            providerBox.Text = item.Provider;
        }

    }
}
