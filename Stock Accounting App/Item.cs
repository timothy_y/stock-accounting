﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

/// <summary>
/// Data of item
/// </summary>
namespace Stock_Accounting_App 
{
    class Item 
    {
        public string Product { get; set; }

        public string Price { get; set; }

        public string Weight { get; set; }

        public string Arrival { get; set; }

        public string Exportation { get; set; }

        public string Provider { get; set; }
    }
}
