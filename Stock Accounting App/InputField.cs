﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Stock_Accounting_App
{
    /// <summary>
    /// Input field for adding new item in table
    /// </summary>
    class InputField :  Grid
    {
        protected TextBox productBox = new TextBox() { VerticalAlignment = VerticalAlignment.Bottom, Margin = new Thickness(0, 0, 0, 0), Height = 32, Width = 80, HorizontalAlignment = HorizontalAlignment.Left, ToolTip = "Product name" };

        protected TextBox priceBox = new TextBox() { VerticalAlignment = VerticalAlignment.Bottom, Margin = new Thickness(110, 0, 0, 0), Height = 32, Width = 80, HorizontalAlignment = HorizontalAlignment.Left, ToolTip = "Price" };

        protected TextBox weightBox = new TextBox() { VerticalAlignment = VerticalAlignment.Bottom, Margin = new Thickness(220, 0, 0, 0), Height = 32, Width = 80, HorizontalAlignment = HorizontalAlignment.Left, ToolTip = "Weight" };

        protected TextBox arrivalBox = new TextBox() { VerticalAlignment = VerticalAlignment.Bottom, Margin = new Thickness(330, 0, 0, 0), Height = 32, Width = 80, HorizontalAlignment = HorizontalAlignment.Left, ToolTip = "Arrival date" };

        protected TextBox exportationBox = new TextBox() { VerticalAlignment = VerticalAlignment.Bottom, Margin = new Thickness(440, 0, 0, 0), Height = 32, Width = 80, HorizontalAlignment = HorizontalAlignment.Left, ToolTip = "Exportation date" };

        protected TextBox providerBox = new TextBox() { VerticalAlignment = VerticalAlignment.Bottom, Margin = new Thickness(550, 0, 0, 0), Height = 32, Width = 80, HorizontalAlignment = HorizontalAlignment.Left, ToolTip = "Provider" };

        private Button saveButton = new Button() { VerticalAlignment = VerticalAlignment.Bottom, Margin = new Thickness(660, 0, 0, 0), Height = 32, Width = 80,
            HorizontalAlignment = HorizontalAlignment.Left, Content = new TextBlock() { Text = "Save" }  };

        public InputField()
        {
            this.VerticalAlignment = VerticalAlignment.Bottom;

            this.Margin = new Thickness(20, 0, 0, 10);

            this.Height = 32;

            //Handling save button click
            saveButton.Click += save;

            this.Children.Add(productBox);
            this.Children.Add(priceBox);
            this.Children.Add(weightBox);
            this.Children.Add(arrivalBox);
            this.Children.Add(exportationBox);
            this.Children.Add(providerBox);
            this.Children.Add(saveButton);

        }

        public virtual void save(object sender, RoutedEventArgs e)
        {
            Global.listView.Items.Add(new Item()
            {
                Product = productBox.Text,
                Price = priceBox.Text,
                Weight = weightBox.Text,
                Arrival = arrivalBox.Text,
                Exportation = exportationBox.Text,
                Provider = providerBox.Text
            });

            //Save to file
            StreamWriter fileWriter = new StreamWriter("Table.txt", true);

            fileWriter.WriteLine(productBox.Text + "\t" + priceBox.Text + "\t" + weightBox.Text + "\t" + arrivalBox.Text + "\t" + exportationBox.Text + "\t" + providerBox.Text);

            fileWriter.Close();

            Global.mainGrid.Children.Remove(this);
            Global.mainGrid.Children.Add(Global.addButton);
        }

    }
}
